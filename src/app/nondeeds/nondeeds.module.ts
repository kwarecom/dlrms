import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NondeedsRoutingModule } from './nondeeds-routing.module';
import { NondeedsComponent } from './nondeeds.component';


@NgModule({
  declarations: [NondeedsComponent],
  imports: [
    CommonModule,
    NondeedsRoutingModule
  ]
})
export class NondeedsModule { }
