import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NondeedsComponent } from './nondeeds.component';

describe('NondeedsComponent', () => {
  let component: NondeedsComponent;
  let fixture: ComponentFixture<NondeedsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NondeedsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NondeedsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
