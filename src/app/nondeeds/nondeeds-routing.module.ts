import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NondeedsComponent } from './nondeeds.component';

const routes: Routes = [{path: '', component: NondeedsComponent}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NondeedsRoutingModule { }
