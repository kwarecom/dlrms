import { HttpClient, HttpHeaders } from '@angular/common/http';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
  ViewChild,
} from '@angular/core';
import { Router } from '@angular/router';
import { SidenavComponent } from 'ng-uikit-pro-standard';
import { Calendar } from 'primeng/calendar';
import { FileUpload } from 'primeng/fileupload';
// import { Projection } from '../services/apis/models';
// import { FileUploadControllerService, ProjectionControllerService } from '../services/apis/services';
// import { AuthService } from '../services/auth-service';
import { baseURL } from '../services/baseUrl';
import { InputValidations } from '../services/validation';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { AppServices } from '../services/app-services';
// import { RetdInfoControllerService } from '../services/apis/services/retd-info-controller.service';

declare var $: any;

@Component({
  selector: 'app-doublenav',
  templateUrl: './doublenav.component.html',
  styleUrls: ['./doublenav.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DoublenavComponent implements OnInit, AfterViewInit {
  @ViewChild('sidenav', { static: true }) sidenav!: SidenavComponent;
  @ViewChild('calendar', {static: false}) private calendar!: Calendar;
  @ViewChild('signatureFile', {static: false}) private signatureFile!: FileUpload;
  // @ViewChild('positionId', {static: false}) private positionId!: Dropdown;

  hasCollapsed!: boolean;
  hospitalRoute = true;
  patientRoute = true;
  labRoute = true;
  pharmacyRoute = true;
  accountRoute = true;
  inventoryRoute = true;
  noticeRoute = true;
  reportRoute = true;
  userRole = 'admin';
  userNames: any = [];
  userData: any;
  toggleType = 'password';
  show = false;
  isLoading!: boolean;
  userId!: string;
  currentUser: any;
  previousRole!: string;
  userName!: string;
  YEAR_RANGE = '2014:' + new Date().getFullYear();
  monthlyProjection: number = 0;
  isProjectionEmpty: boolean = false;
  comment = "";
  projectionMonth!: Date;
  isDateIvalid: boolean = false;
  editData: boolean = false;
  editableId: any;
  defaultLoading!: boolean;

  showProjectionDialog: boolean = false;
  showRoleUpdateDialog: boolean = false;
  showAccountCreationDialog: boolean = false;

  newUserRole: any;
  newUserStatus: any;
  fullName: any;
  email: any;
  userRoleForm: boolean = false;
  userAccountForm: boolean = false;
  editAccountForm: boolean = false;
  isEmailValid: boolean = false;
  isMobileValid: boolean = false;
  isPasswordValid: boolean = false;
  isUserNameInValid: boolean = false;
  isUserRoleInValid: boolean = false;
  errMsgEmail!: string;
  errMsgPassword!: string;
  invalidPassword!: boolean;
  errMsgMobile!: string;
  errEditedMsgMobile!: string;

  password!: string;
  officeLocation!: string;
  userSection!: any;
  mobile!: string;
  userEmail!: string;
  editedMobile!: string;
  editedSection!: string;
  editedOffice!: string;
  editedEmail!: string;
  nameUser!: string;
  editedName!: string;
  userOffice!: any;

  userLogout = false;

  confirmPassword!: string;
  oldPassword!: string;
  newPassword!: string;
  errMsgOldPassword!: string;
  passwordForm!: boolean;
  errMsgNewPassword!: string;
  errMsgConfirmPassword!: string;
  displayChangePasswordDialog = false;
  displayUserAccountDialog = false;
  displaySignatoryDialog = false;
  imageChangedEvent: any = '';
  // croppedImage: any = '';
  userImgUrl!: string;
  userImg: any = '';
  fileName!: string;
  // userAvatar!: string;

  signatoryName!: string;
  signatoryPosition!: any;
  signatoryContact!: string;
  signatorySignature!: string;
  isSignatoryNameInValid!: boolean;
  isSignatoryContactInValid!: boolean;
  isSignatoryPositionInValid!: boolean;
  isSignatureLoading!: boolean;
  signature: any[] = [];

  showImagePreview: boolean = false;

  commitBtn = 'Save';

  userRoleList = [
    { value: 'Accounts', label: 'Accounts' },
    { value: 'Administration', label: 'Administration' },
    { value: 'Enforcement', label: 'Enforcement' },
    { value: 'Local Government', label: 'Local Government' },
    { value: 'RETD AC', label: 'RETD AC' },
    { value: 'Valuation', label: 'Valuation' },
    { value: 'admin', label: 'admin' },
  ];
  userStatusList = [
    { value: 'Active', label: 'Active' },
    { value: 'Inactive', label: 'Inactive' },
  ];
  positions = [
    { value: 'Assistant Commissioner', label: 'Assistant Commissioner' },
    { value: 'Commissioner', label: 'Commissioner' },
    { value: 'Enforcement Manager', label: 'Enforcement Manager' },
  ];
  userSections = [
    { value: 'Accounts', label: 'Accounts' },
    { value: 'Enforcement', label: 'Enforcement' },
    { value: 'Local Government', label: 'Local Government' },
    { value: 'Administration', label: 'Administration' },
    { value: 'Valuation', label: 'Valuation' },
  ];
  userOfficeLocation = [
    { value: 'HQ', label: 'HQ' },
    // { value: 'Bomi', label: 'Bomi' },
    // { value: 'Bong', label: 'Bong' },
    // { value: 'Gbarpolu', label: 'Gbarpolu' },
    // { value: 'Grand Bassa', label: 'Grand Bassa' },
    // { value: 'Grand Cape Mount', label: 'Grand Cape Mount' },
    // { value: 'Grand Kru', label: 'Grand Kru' },
    // { value: 'Grand Gedeh', label: 'Grand Gedeh' },
    // { value: 'Lofa', label: 'Lofa' },
    { value: 'Margibi', label: 'Margibi' },
    // { value: 'Maryland', label: 'Maryland' },
    // { value: 'Montserrado', label: 'Montserrado' },
    // { value: 'Nimba', label: 'Nimba' },
    // { value: 'River Gee', label: 'River Gee' },
    // { value: 'Rivercess', label: 'Rivercess' },
    // { value: 'Sinoe', label: 'Sinoe' },
  ];

  constructor(
    private router: Router,
    // public auth: AuthService,
    public validator: InputValidations,
    private cdr: ChangeDetectorRef,
    // private projection: ProjectionControllerService,
    private http: HttpClient,
    // private uploadImg: FileUploadControllerService,
    private service: AppServices,
    // private signatory: RetdInfoControllerService
  ) {
    this.userImgUrl = baseURL;
  }

  ngOnInit(): void {
    // this.currentUser = this.auth.getCurrentlyLoggedInUser();
    this.toggleCollapse(this.router.url);
    // this.currentUser?.user?.roles === 'admin' ? this.auth.findAllUsers().then((users: any) => {
    //   users.map((user: any) => {
    //     const option: any = { value: user.fullName, label: user.fullName };
    //     this.userNames.push(option);
    //   });
    //   this.userData = users;
    // }) : ''
  }
  displayPassword() {
    if (this.show) {
      this.toggleType = 'password';
    } else {
      this.toggleType = 'text';
    }
    this.show = !this.show;
  }
  ngAfterViewInit(): void {
    const that = this;
    $('#test-nav').hover(() => {
      if (that.hasCollapsed) {
        // $('.adjust-img').toggleClass('img-toggle-padding');
        that.sidenav.toggleSlim();
      }
    });
  }

  toggleMenu() {
    this.hasCollapsed = !this.hasCollapsed;
    $('#footer-slim').toggleClass('slim-footer toggle-slim-nav ');
    $('main').toggleClass('slim-main toggle-slim-nav ');
    // $('.adjust-img').toggleClass('img-toggle-padding');
    $('.social').toggleClass('icon-padding');
    this.sidenav.toggleSlim();
    $('.scrolling-navbar').toggleClass('toggle-slim-nav');
    $('.custom-container').toggleClass('toggle-slim-nav');
    $('.getSideNav').toggleClass('pi-angle-double-left pi-angle-double-right');
  }

  private toggleCollapse(url: string) {
    if (url) {
      if (url.includes('hospital')) {
        this.hospitalRoute = false;
      } else if (url.includes('patient') && !url.includes('report')) {
        this.patientRoute = false;
      } else if (url.includes('laboratory')) {
        this.labRoute = false;
      } else if (url.includes('pharmacy')) {
        this.pharmacyRoute = false;
      } else if (url.includes('account')) {
        this.accountRoute = false;
      } else if (url.includes('supplies')) {
        this.inventoryRoute = false;
      } else if (url.includes('report')) {
        this.reportRoute = false;
      } else if (url.includes('notice')) {
        this.noticeRoute = false;
      }
    }
  }
  getSelectedValue(event: any) {
    const index = this.userData.findIndex(
      (x: any) => x.fullName === event.value.label
    );
    if(index != -1) {
    this.email = this.userData[index].email;
    this.newUserRole = { value: this.userData[index].roles, label: this.userData[index].roles };
    this.newUserStatus = { value: this.userData[index].userStatus, label: this.userData[index].userStatus };
    this.previousRole = this.userData[index].userrole;
    this.userId = this.userData[index].id;
    this.userName = this.userData[index].username;
    this.isUserNameInValid = false;
    }
  }
  getSelectedSection(event: any) {
      this.userSection === event.value.label
  }
  handleSelection(event: any) {
      this.userOffice === event.value.label
  }
  handleStatusSelection(event: any) {
      this.newUserStatus === event.value.label
  }

  createAccount() {
    // this.isLoading = true
        this.userAccountForm = (!this.nameUser || !this.userEmail || !this.userSection?.label || !this.password || !this.userOffice?.label || !this.mobile) ? true : false;
        this.errMsgEmail = !this.userEmail ? 'Email must be provided!' : !this.userEmail.match(this.validator.emailRegex()) ? 'Email is invalid!' : '';
        this.errMsgMobile = !this.mobile ? 'User mobile is required!' : !this.mobile.match(this.validator.phoneRegex()) ? 'Phone number is invalid!' : '';
        this.errMsgPassword = !this.password ? 'Password is required!' : !this.password.match(this.validator.mediumPasswordRegex()) ? 'Password is invalid!' : '';
        this.invalidPassword = this.validator.mediumPasswordRegex().test(this.password);
    if (!this.userAccountForm && this.invalidPassword)  {
      // console.log(this.invalidPassword);
      // console.log(this.userOffice)
      try {
        const data = {
                name: this.nameUser,
                email: this.userEmail,
                password: this.password,
                mobile: this.mobile,
                section: this.userSection,
                office: this.userOffice
        }
        
        // this.auth.signUp(data, this.cdr)
        // .then((res: any) => {
        //   if(this.auth.uniqueAccount) {
        //  this.nameUser = '';
        //  this.userEmail = '';
        //   this.password = '';
        //   this.mobile = '';
        //   this.userSection = null
        //   this.userOffice = null;
        //   }
        // })
      } finally {
      }
    }
  }
  updateUserRole() {
    this.isLoading = true;
    this.userRoleForm = (!this.newUserRole || !this.email || !this.fullName) ? true : false;
    this.isUserNameInValid = !this.fullName ? true : false;
    this.isUserRoleInValid = !this.newUserRole ? true : false;

    if(!this.userRoleForm) {
      try {
        const data = {
          id: this.userId,
          body: {
          roles: this.newUserRole.label,
          userStatus: this.newUserStatus
          }
        };
        // this.auth.updateRoles(data).then((res: any) => {
        //   this.isLoading = false;
        //   this.cdr.markForCheck();
        // });
        this.newUserRole = null;
        this.fullName = '';
        this.email = '';
      } finally {
      }
    } else {
      this.isLoading = false;
      this.cdr.markForCheck();
    }
  }

  displayProjectForm() {
    this.showRoleUpdateDialog = false;
    this.showAccountCreationDialog = false;
    this.displayChangePasswordDialog = false;
    this.displayUserAccountDialog = false;
    this.displaySignatoryDialog = false;
    this.showProjectionDialog = !this.showProjectionDialog;
    this.isProjectionEmpty = false;
    this.isDateIvalid = false;
    this.monthlyProjection = 0;
    this.projectionMonth;
  }
  displayRoleUpdateForm() {
    this.showProjectionDialog = false;
    this.showAccountCreationDialog = false;
    this.displayUserAccountDialog = false;
    this.displaySignatoryDialog = false;
    this.displayChangePasswordDialog = false;
    this.showRoleUpdateDialog = !this.showRoleUpdateDialog;
  }
  displayAccountForm() {
    this.showProjectionDialog = false;
    this.showProjectionDialog = false;
    this.displayUserAccountDialog = false;
    this.displaySignatoryDialog = false;
    this.displayChangePasswordDialog = false;
    this.showAccountCreationDialog = !this.showAccountCreationDialog;
  }

  async saveProjectionData() {
    this.isProjectionEmpty = this.monthlyProjection === 0 ? true : false;
    this.isDateIvalid = !this.projectionMonth ? true : false;

    if(!this.isDateIvalid && !this.isProjectionEmpty) {
      this.isLoading = true;
      if(this.editData) {
        // this.projection.updateById({id: this.editableId, 
        //   body: {amount: this.monthlyProjection, comments: this.comment}})
        //   .subscribe((res: any) => {
        //     this.monthlyProjection = 0;
        //     this.comment = "";
        //     this.calendar.value = null;
        //     this.calendar.updateInputfield();
        //     this.isLoading = false;
        //     this.editData = false;
        //     this.editableId = '';
        //     this.cdr.markForCheck();
        //     this.auth.showSuccessToast('Reecord successfully updated', 'Success');
        //   });
      } else {
        this.checkProjectionDate();
    }
    }
  }

  validateDateInput() {
    this.isDateIvalid = !this.projectionMonth ? true : false;
    if(this.editData) {
      // this.http.get<Projection>(baseURL + '/projections?filter[where][month][eq]=' + this.projectionMonth.toISOString().slice(0, 10))
      // .subscribe((res: any) => {
      //   if(res?.length > 0) {
      //     this.commitBtn = 'Update';
      //   this.monthlyProjection = res[0].amount;
      //   this.comment = res[0].comments;
      //   this.editableId = res[0].id;
      //   } else {
      //     this.monthlyProjection = 0;
      //   this.comment = '';
      //   this.commitBtn = 'Update';
      //   this.editableId = -1;
      //     this.auth.showWarningToast('No record found for ' + this.projectionMonth.toLocaleString("default", {month: "long"}), 'Info');
      //   }
      // })
    }
  }
  validateInput(e: any) {
    this.isProjectionEmpty = Number(e.value) > 0 ? false : true;
  }
  handleBlur(e: any) {
    this.errMsgPassword = !e.target.value ? 'Password is required!' : !this.validator.mediumPasswordRegex().test(e.target.value) ? 'Password is invalid!' : '';
  }
  handleOldPasswordBlur(e: any) {
    this.errMsgOldPassword = !e.target.value ? 'Old password is required!' : '';
  }
  handleNewPasswordBlur(e: any) {
    this.errMsgNewPassword = !e.target.value ? 'New password is required!' : !e.target.value.match(this.validator.strongPasswordRegex()) ? 'Password is invalid!' : '';
  }
  handleConfirmPasswordBlur(e: any) {
    this.errMsgConfirmPassword = !e.target.value ? 'Confirm password is required!' : this.newPassword.localeCompare(e.target.value) !== 0 ? 'Password mismatched!' : '';
  }
  handleNameChange(e: any) {
    this.nameUser = !e.target.value ? 'User fUll name is required!' : '';
  }
  handleAccountNameChange(e: any) {
    this.editedName = !e.target.value ? 'User fUll name is required!' : '';
  }

  logUserOut = () => {
    this.userLogout = !this.userLogout;
  } 

  changePassword = () => {
    this.passwordForm = !this.oldPassword || !this.confirmPassword || !this.newPassword;
    this.errMsgOldPassword = !this.oldPassword ? 'Old password is required!' : '';
    this.errMsgNewPassword = !this.newPassword ? 'New password is required!' : !this.newPassword.match(this.validator.strongPasswordRegex()) ? 'Password is invalid!' : '';
    this.errMsgConfirmPassword = !this.confirmPassword ? 'Confirm password is required!' : this.newPassword.localeCompare(this.confirmPassword) !== 0 ? 'Password mismatched!' : '';

    if (!this.passwordForm) {
      const data = {
        // id: this.auth.getCurrentlyLoggedInUser()?.user.id,
        oldPassword: this.oldPassword,
        newPassword: this.newPassword
      }
      let headers = new HttpHeaders();
      // headers = headers.set('Authorization', 'Bearer ' + this.auth.getCurrentlyLoggedInUser()?.token);
      // this.http.post(baseURL + '/user/change-password', data, {headers: headers})
      // .subscribe((res: any) => {
      //   this.auth.showSuccessToast('Password change request successful!', 'Success');
      //   localStorage.removeItem('credentials');
      //   setTimeout(() => {       
      //     this.auth.routeToPage('auth-page');
      //   }, 6500);
      // }, (err: any) => {
      //   console.log(err)
      //   if (err.statusText === 'Unauthorized') {
      //     this.auth.showErrorToast('Operation denied: Unauthorized user', 'ACCESS DENIED');
      //   } else {
      //     this.auth.showErrorToast('Wrong old password!', 'Error');
      //   }
      // })
    } 
  }

  showChangePassword = () => {
    this.showProjectionDialog = false;
    this.showAccountCreationDialog = false;
    this.showRoleUpdateDialog = false;
    this.displayUserAccountDialog = false;
    this.displaySignatoryDialog = false;
    this.displayChangePasswordDialog = !this.displayChangePasswordDialog
  }

   checkProjectionDate = () => {
    //  this.http.get<Projection>(baseURL + '/projections?filter[where][month][eq]=' + this.projectionMonth.toISOString().slice(0, 10))
    //  .subscribe((res: any) => {
    //   if(res?.length > 0) {
    //     this.auth.showErrorToast(this.projectionMonth.toLocaleDateString("en-US", {month: "long"}) +' projection already exist!', 'Forbidden');
    //     this.isLoading = false;
    //     this.cdr.markForCheck();
      // } else {
        // this.projection.create({body: {
        //   amount: this.monthlyProjection,
        //   month: this.projectionMonth.toISOString(),
        //   comments: this.comment
        // }}).subscribe((res: any) => {
        //   this.monthlyProjection = 0;
        //   this.comment = "";
        //   this.commitBtn = 'Save';
        //   this.calendar.value = null;
        //   this.calendar.updateInputfield();
        //   this.isLoading = false;
        //   this.cdr.markForCheck();
        //   this.auth.showSuccessToast('Reecord successfully added', 'Success');
        // });
    //   }
    // })
  }

  showUserAccountDialog = () => {
    this.showProjectionDialog = false;
    this.showAccountCreationDialog = false;
    this.showRoleUpdateDialog = false;
    this.displayChangePasswordDialog = false;
    this.displayUserAccountDialog = !this.displayUserAccountDialog;

    const accountEmail = this.currentUser?.user.email;
    // this.auth.findUniqueUsers(accountEmail)
    // .then((user: any) => {
    //   if(user?.length > 0) {
    //   this.userImg = this.userImgUrl + '/files/' + user[0].avatar;
    //   this.editedName = user[0].fullName;
    //   this.editedEmail = user[0].email;
    //   this.editedMobile = user[0].contact;
    //   this.editedOffice = user[0].office;
    //   this.editedSection = user[0].roles;
    //   this.fileName = user[0].avatar;
    //   this.userId = user[0].id;
    //   }
    // });
  }
  updateUserAccount() {
    this.editAccountForm = (!this.editedName || !this.editedMobile) ? true : false;
        this.errEditedMsgMobile = !this.editedMobile ? 'User mobile is required!' : !this.editedMobile.match(this.validator.phoneRegex()) ? 'Phone number is invalid!' : '';
    if (!this.editAccountForm)  {
      try {
        const data = {
          id: this.userId,
          body: {
            fullName: this.editedName,
            contact: this.editedMobile,
            avatar: this.fileName ? this.fileName : 'blank-silhouette.jpg'
          }
        };
        // this.auth.updateUserAccount(data)
        // .then((res: any) => {
          if(this.fileName) {
            // this.uploadImg.fileUpload({body: {file: this.service.base64ToBlob(this.userImg, this.fileName)}})
            // .subscribe((file: any) => {
            //   this.editedName = '';
            //   this.editedMobile = '';
            //   this.editedEmail ='';
            //   this.editedMobile ='';
            //   this.editedOffice = '';
            //   this.editedSection = '';
            //   const userDetail = this.currentUser;
            //   userDetail.avatar = this.fileName;
            //   localStorage.setItem('credentials', JSON.stringify(userDetail));
            //   this.currentUser = this.auth.getCurrentlyLoggedInUser();
            //   this.fileName = '';
            //   this.cdr.markForCheck();
            // });
          } else {
            this.editedName = '';
            this.editedMobile = '';
            this.fileName = '';
            this.editedEmail ='';
            this.editedMobile ='';
            this.editedOffice = '';
            this.editedSection = '';
          }
        // });
      } finally {
      }
    } else {
      this.isLoading = false;
      this.cdr.markForCheck();
    }
}

onImgError(event: any){
  // event.target.src = this.userImgUrl + '/files/' + 'blank-silhouette.jpg'
  event.target.src = 'assets/images/blank-silhouette.jpg'
 }
 imageCropped(event: ImageCroppedEvent) {
  this.userImg = event.base64;
}
fileChangeEvent(event: any): void {
  const file = event.target.files;
  if(!file[0] || file.length === 0) {
    // this.auth.showErrorToast('You must select an image', 'Error');
    this.userImg = this.userImgUrl + '/files/' + 'blank-silhouette.jpg';
    return;
  }
  if (file[0].type.match(/image\/*/) == null) {
    // this.auth.showErrorToast('Only images are supported', 'Error');
    this.userImg = this.userImgUrl + '/files/' + 'blank-silhouette.jpg';
    return;
  }
  if (file[0].size > 75000) {
    // this.auth.showErrorToast('Large file size! Requires < 75Kb.', 'Error');
    this.userImg = this.userImgUrl + '/files/' + 'blank-silhouette.jpg';
    return;
  }
  this.imageChangedEvent = event;
  this.fileName = this.currentUser?.user?.email + '.png';
  this.showImagePreview = !this.showImagePreview
}
handleDisplay() {
  this.showImagePreview = !this.showImagePreview
}
displaySignatoryForm() {
  this.showProjectionDialog = false;
    this.showAccountCreationDialog = false;
    this.displayUserAccountDialog = false;
    this.displayChangePasswordDialog = false;
    this.showRoleUpdateDialog = false;
    this.displaySignatoryDialog = !this.displaySignatoryDialog;
}
onSelect(event: any) {
  this.signature = event.currentFiles;
}

updateOrCreateInfo(e: any) {
  this.isSignatureLoading = true;
  
  // this.http.get(baseURL + '/retd-infos?filter[where][position][eq]=' + this.signatoryPosition.label)
  // .subscribe((c: any) => {
    // if(c?.length > 0) {
      const data = {contact: this.signatoryContact, name: this.signatoryName}
      // this.signatory.updateById({id: c[0].id, body: data})
      // .subscribe((res: any) => {
      //   if(this.signature.length > 0) {
      //     this.saveSIgnature(this.signature[0], this.signatoryPosition.label)
      //     .then((val: any) => {
      //       this.auth.showSuccessToast('Record updated successfully!', 'Success');
      //     });
      //   } else {
      //     this.auth.showSuccessToast('Record updated successfully!', 'Success');
      //       this.signatoryPosition = null;
      //       this.signatoryContact = '';
      //       this.signatoryName = '';
      //       this.isSignatureLoading = false; 
      //       this.cdr.markForCheck();
      //   }
      // }, (err: any) => {
      //   this.auth.showErrorToast('No record added! Try again', 'Error');
      //   this.isSignatureLoading = false;
      //   this.cdr.markForCheck();
      // });
    // } else {
  //     this.signatory.create({body: {contact: this.signatoryContact, name: this.signatoryName, position: this.signatoryPosition.label, signature: this.signatoryPosition.label +'.png'}})
  //     .subscribe((res: any) => {
  //       if(this.signature.length > 0) {
  //       this.saveSIgnature(this.signature[0], this.signatoryPosition.label)
  //       .then((val: any) => {
  //         this.auth.showSuccessToast('Record added successfully!', 'Success');
  //       });
  //     } else {
  //       this.auth.showSuccessToast('Record added successfully!', 'Success');
  //         this.signatoryPosition = null;
  //         this.signatoryContact = '';
  //         this.signatoryName = '';
  //         this.isSignatureLoading = false;
  //         this.cdr.markForCheck();
  //     }
  //     }, (err: any) => {
  //       this.auth.showErrorToast('No record added! Try again', 'Error');
  //       this.isSignatureLoading = false;
  //       this.cdr.markForCheck();
  //     })
  //   }
  // })
// }
// async saveSIgnature(newFile: any, name: string) {
//   const file = new File([newFile], name + '.png', {type: 'image/png'});

//   this.uploadImg.fileUpload({body: {file: file}})
//   .subscribe((f: any) => {
//     this.signatoryPosition = null;
//     this.signatoryContact = '';
//     this.signatoryName = '';
//     this.isSignatureLoading = false;
//     this.signatureFile.clear();
//     this.cdr.markForCheck();
//   });
} 

userPartition(role: string, route: string): boolean {
  let display = false;
  if((role === 'Accounts' || role === 'Enforcement' || role === 'Administration' || role === 'RETD AC' || role === 'Local Government' || role === 'Valuation') && route === 'Owner Information') {
    display = true;
  } else if((role === 'Accounts' || role === 'Enforcement' || role === 'Administration' || role === 'RETD AC' || role === 'Valuation') && route === 'RETD Dashboard') {
    display = true;
  } else if(role === 'Accounts' && route === 'Engagements') {
    display = true;
  } else if(role !== 'admin' && route === 'Reports') {
    display = true;
  } else if((role === 'Valuation' || role === 'Enforcement') && route === 'Notices') {
    display = true;
  }
  return display
}

subPartition(role: string, link: string): boolean {
  let display = false;
  if (role === 'Valuation' && link === 'Request To File') {
    display = true
  } else if (role === 'Enforcement' && link === 'Determination') {
    display = true
  } else if (role !== 'Local Government' && link === 'RETD Monthly') {
    display = true
  } else if (role !== 'Local Government' && link === 'RETD YTD') {
    display = true
  } else if (role !== 'Local Government' && link === 'RETD Tax Roll') {
    display = true
  } else if (role !== 'Local Government' && link === 'RETD Bad Debt') {
    display = true
  } else if (role !== 'Local Government' && link === 'Registered Owners') {
    display = true
  }
  return display;
}
}
