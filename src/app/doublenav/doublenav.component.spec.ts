import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DoublenavComponent } from './doublenav.component';

describe('DoublenavComponent', () => {
  let component: DoublenavComponent;
  let fixture: ComponentFixture<DoublenavComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DoublenavComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DoublenavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
