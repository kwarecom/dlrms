import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { InputValidations } from './validation';

@Injectable({ providedIn: 'root' })
export class AppServices {
  constructor(
    private title: Title,
    private http: HttpClient,
    private validator: InputValidations,
    private router: Router
  ) {}

  setBrowserTabTitle(title: string) {
    this.title.setTitle(title + ' - LLA DLRMS');
  }

  getFormObject = (form: any) =>
    Array.from(new FormData(document.querySelector(form))).reduce(
      (acc, [key, value]) => ({
        ...acc,
        [key]: value,
      }),
      {}
    );
  routeToPage(page: string) {
    this.router.navigate([page]);
  }

  navigateToPage(route: string, params: any) {
    this.router.navigate([route], { queryParams: { Param: params } });
  }
  pageLoader() {
    setTimeout(() => {
      $('.preloader').addClass('preloader-deactivate');
    }, 1000);
  }
  loadingSpinner() {
    setTimeout(() => {
      $("#loader").addClass("fadeOut");
    }, 500);
  }
  formatNumber(number: any) {
    return new Intl.NumberFormat('en-US', {
      // style: 'currency',
      currency: 'USD',
      minimumFractionDigits: number % 1 != 0 ? 2 : 0
    }).format(number);
  }
}
