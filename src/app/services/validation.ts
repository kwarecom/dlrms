import { Injectable, Input } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class InputValidations {
  @Input() registrationEnabled = true;
  @Input() resetPasswordEnabled = true;
  @Input() messageOnEmailConfirmationSuccess =
    'Success: Email was sent and successfully delivered';

  // Password strength api
  @Input() enableLengthRule = true;
  @Input() enableLowerCaseLetterRule = true;
  @Input() enableUpperCaseLetterRule = true;
  @Input() enableDigitRule = true;
  @Input() enableSpecialCharRule = true;

  // i18n translations to use in default template for email verification.
  // See email-confirmation component
  @Input() verifyEmailTitleText = 'Confirm your e-mail address!';
  @Input() verifyEmailConfirmationText =
    'A confirmation e-mail has been sent to you. Check your inbox and click on the verification link to confirm your email address.';
  @Input() verifyEmailGoBackText = 'Click here to continue';
  @Input() sendNewVerificationEmailText!: string;
  @Input() signOutText = 'Sign out';

  // Customize the text
  // Reset Password Tab
  @Input() resetPasswordTabText = 'Reset e-mail address to password';
  @Input() resetPasswordInputText = 'Reset e-mail address to password';
  @Input() resetPasswordErrorRequiredText =
    'E-mail is required to reset the password!';
  @Input() resetPasswordErrorPatternText =
    'Please enter a valid e-mail address';
  @Input() resetPasswordActionButtonText = 'Reset';
  @Input() resetPasswordInstructionsText =
    'Reset requested. Check your e-mail instructions.';

  @Input() resetCardTitleText = 'Resetting password';
  @Input() changeCardTitleText = 'Changing password';
  @Input() resetButtonText = 'Reset password';
  @Input() changeButtonText = 'Change password';
  @Input() passwordConfirmText = 'Confirm password';
  @Input() newPasswordText = 'New password';
  @Input() confirmationErrorRequiredText = 'Confirm password is required!';
  @Input() newPasswordErrorRequiredText = 'New password is required!';
  @Input() confirmError = 'Password mismatched!';
  @Input() sameChangedPasswordError = 'Passwords are the same!';

  // SignIn Tab
  @Input() signInTabText = 'Sign in';
  @Input() signInCardTitleText = 'Signing in';
  @Input() loginButtonText = 'Log In';
  @Input() forgotPasswordButtonText = 'Forgot Password ?';

  // Common
  @Input() nameText = 'Name';
  @Input() errorRequiredText = ' is required!';
  @Input() nameErrorMaxLengthText = 'The name is too long!';
  @Input() cNameErrorRequiredText = 'City name is required';
  @Input() ctryNameErrorRequiredText = 'country name is required';
  @Input() sNameErrorRequiredText = 'State name is required';
  @Input() nameErrorPatternText = 'Name cannot contain numbers!';
  @Input() mNameErrorRequiredText = 'Middle name is required';
  @Input() dobErrorRequiredText = 'Date of birth is required';
  @Input() dobMinErrorText = 'Complainant must be an adult(18 & above)';
  @Input() genderErrorRequiredText = 'Gender is required';
  @Input() addressErrorRequiredText = 'Address is required';
  @Input() countyErrorRequiredText = 'County is required';
  @Input() salaryErrorRequiredText = 'Employee salary is required!';

  @Input() emailText = 'E-mail';
  @Input() emailErrorRequiredText = 'E-mail is required!';
  @Input() emailErrorPatternText = 'Invalid e-mail address!';
  @Input() credentialRequiredText = 'Login credential is required!';

  @Input() passwordText = 'Password';
  @Input() oldPasswordText = 'Old password';
  @Input() passwordErrorRequiredText = 'Password is required!';
  @Input() passwordErrorMinLengthText =
    'Password must be atleast 6 characters!';
  @Input() passwordErrorMaxLengthText = 'The password is too long!';
  @Input() passwordPatternError =
    'Password must contain atleast 1 upper case, lower case, a numeber and a special character';
  @Input() passwordMismatched = 'Password must be the same!';

  @Input() phoneNumRequired = 'Phone number is required!';
  @Input() phoneNumMinLength = 'Requires atleast 10 digits!';

  // Register Tab
  @Input() registerTabText = 'Register';
  @Input() registerCardTitleText = 'Registration';
  @Input() registerButtonText = 'Register';
  @Input() guestButtonText = 'continue as guest';

  // email confirmation component
  @Input() emailConfirmationTitle = 'Confirm your e-mail address!';
  @Input() usernameErrorRequiredText = 'Username is required!';
  @Input() usernameErrorMinLengthText = 'Requires atleast 4 chars!';

  @Input() userRoleErrorRequiredText = 'User role is required!';
  @Input() titleErrorRequiredText = 'User title is required!';
  @Input() phoneErrorRequiredText = 'Phone number is required!';
  @Input() phonePatternError =
    'INVALID: Must be 0111-222333, 00231-111-222333 or +231-111-222333';

  @Input() nameMaxLength = 50;
  @Input() nameMinLength = 2;
  @Input() passwordLength = 0;
  @Input() minUsernameLength = 4;
  @Input() minLength = 6;
  @Input() maxLength = 25;

  @Input() lawyerErrorRequiredText = 'Please select lawyer';

  strongPasswordRegex = () => {
    // return new RegExp(
    //   '^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*?[#@$!%*?&`~^-_]).{6,25}$'
    // );
    return /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*?[#@$!%*?&`~^-_]).{6,25}$/;
    // return /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{6,})/
  }
  mediumPasswordRegex = () => {
    return /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{6,})/;
    // return /^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{4,})/;
  }
  emailRegex = () => {
    return new RegExp(
      [
        '^(([^<>()[\\]\\.,;:\\s@"]+(\\.[^<>()\\[\\]\\.,;:\\s@"]+)*)',
        '|(".+"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.',
        '[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)+',
        '[a-zA-Z]{2,}))$',
      ].join('')
    );
    // return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
  }
  minLengthRegex() {
    return new RegExp(/[0-9a-zA-Z]{2,}/);
  }
  minUsernameLengthRegex() {
    return new RegExp(/[0-9a-zA-Z]{4,}/);
  }
  phoneRegex = () => {
    return new RegExp(
      '^((\\+231)|0)[.\\- ]?[0-9][.\\- ]?[0-9][.\\- ]?[0-9][.\\- ]?[0-9][.\\- ]?[0-9][.\\- ]?[0-9][.\\- ]?[0-9][.\\- ]?[0-9][.\\- ]?[0-9]'
    );
  }
  nameChecker() {
    return new RegExp('/d/');
  }
}
