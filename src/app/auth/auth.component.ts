import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  AbstractControl,
  FormControl,
  Validators,
} from '@angular/forms';
// import { AuthService } from '../services/auth-service';
import { AppServices } from '../services/app-services';
import { InputValidations } from '../services/validation';
declare var $: any;

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
})
export class AuthComponent implements OnInit {
  public loginFormGroup!: FormGroup;
  public loginEmailControl!: AbstractControl;
  public loginPasswordControl!: AbstractControl;

  public resetFormGroup!: FormGroup;
  public resetEmailControl!: AbstractControl;

  public requestFormGroup!: FormGroup;
  public requestEmailControl!: AbstractControl;

  count: number = 0;
  isLoading!: boolean;
  isVisible!: boolean;
  showPasswordReset!: boolean;

  public subjectOptionText!: string;
  public userEmail!: string;
  public currentYear = new Date().getFullYear();
  public togglePassword = 'password';
  public toggleIcon = '';

  constructor(
    public validator: InputValidations,
    // public auth: AuthService,
    private service: AppServices,
  ) {}

  public ngOnInit(): void {
    this.initFormControls();
    this.initResetFormControls();
    this.initRequestFormControls();
    this.service.setBrowserTabTitle('User Authentication');
    $('#signin').toggleClass('hide-content');
    $('#signin').addClass('active');
    const reset: any = localStorage.getItem('resetCount');
    if(reset?.date) {
      if(new Date(reset?.date).getDay() === new Date().getDay())
      this.count = Number(reset?.count);
    }
  }

  public initFormControls(): void {
    this.loginFormGroup = new FormGroup({
      email: (this.loginEmailControl = new FormControl('', [
        Validators.required,
      ])),
      password: (this.loginPasswordControl = new FormControl('', [
        Validators.required,
      ])),
    });
  }
  
  loadPage(ids: string) {
    const thisPage = this;
    $('#pagecontent > div').map((id: any) => {
      if (id !== ids && $('#' + id).hasClass('active')) {
        $('#' + id).removeClass('active');
        $('#' + id).toggleClass('hide-content');
        return;
      }
    });

    $('#' + ids).addClass('active');
    $('#' + ids).toggleClass('hide-content');
  }

  handleFormToggle() {
    this.isVisible = !this.isVisible;
  }
  handleToggleReset() {
    this.showPasswordReset = !this.showPasswordReset;
  }

  onChange($event: any) {
    this.subjectOptionText =
      $event.target.options[$event.target.options.selectedIndex].text;
  }

  public initResetFormControls(): void {
    this.resetFormGroup = new FormGroup({
      resetEmail: (this.resetEmailControl = new FormControl('', [
        Validators.required,
        Validators.pattern(this.validator.emailRegex()),
      ])),
    });
  }
  public initRequestFormControls(): void {
    this.requestFormGroup = new FormGroup({
      requestingEmail: (this.requestEmailControl = new FormControl('', [
        Validators.required,
        Validators.pattern(this.validator.emailRegex()),
      ])),
    });
  }

  userLogin() {
    let isValid = false;
    if (this.loginFormGroup.invalid) {
      Object.keys(this.loginFormGroup.controls).forEach((key) => {
        if (this.loginFormGroup.controls[key].invalid) {
          this.loginFormGroup.controls[key].markAsTouched();
          isValid = true;
        } else {
          isValid = false;
        }
      });
    } 
    if (!isValid) {
      console.log('login successful');
      this.service.routeToPage('dashboard');
        // this.auth
        // .signIn(this.loginFormGroup);
    } 
  }

  resendEmail() {
    if (this.count < 4) {
    let isValid = false;
    if (this.resetFormGroup.invalid) {
      Object.keys(this.resetFormGroup.controls).forEach((key) => {
        if (this.resetFormGroup.controls[key].invalid) {
          this.resetFormGroup.controls[key].markAsTouched();
          isValid = true;
        } else {
          isValid = false;
        }
      });
    } 
    if(!isValid) {
      console.log('Email successfully sent');
    // this.auth.resetPassword(this.resetEmailControl.value)
    // .then((response: any) => {
    //   this.count += 1;
    //   localStorage.setItem('resetCount', JSON.stringify({count: this.count.toString(), date: new Date()}));
    //   this.loadPage('verifyCode');
    // });
    }
  } else {
    console.log('Error sending email');
      // this.auth.showErrorToast('Account has reached daily limit for sending password-reset requests', 'Unauthorized!')
    }
  }
  resetPassword() {
    if(this.count < 4) {
    let isValid = false;
    if (this.resetFormGroup.invalid) {
      Object.keys(this.resetFormGroup.controls).forEach((key) => {
        if (this.resetFormGroup.controls[key].invalid) {
          this.resetFormGroup.controls[key].markAsTouched();
          isValid = true;
        } else {
          isValid = false;
        }
      });
    } 
    if(!isValid) {
      console.log('Password reset successful');
    // this.auth.resetPassword(this.resetEmailControl.value)
    // .then((response: any) => {
    //   this.count += 1;
    //   localStorage.setItem('resetCount', JSON.stringify({count: this.count.toString(), date: new Date()}));
    //   this.loadPage('verifyCode');
    // });
    }
  } else {
    console.log('Error resetting password');
    // this.auth.showErrorToast('Account has reached daily limit for sending password-reset requests', 'Unauthorized!')
  }
  }

  togglePasswordDisplay() {
    this.togglePassword =
      this.togglePassword === 'password' ? 'text' : 'password';
  }
}
