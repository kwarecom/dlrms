import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SpatialRoutingModule } from './spatial-routing.module';
import { SpatialComponent } from './spatial.component';


@NgModule({
  declarations: [SpatialComponent],
  imports: [
    CommonModule,
    SpatialRoutingModule
  ]
})
export class SpatialModule { }
