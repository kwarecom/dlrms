import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SpatialComponent } from './spatial.component';

const routes: Routes = [{path: '', component: SpatialComponent}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SpatialRoutingModule { }
