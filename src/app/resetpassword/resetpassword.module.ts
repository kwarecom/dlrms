import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ResetpasswordRoutingModule } from './resetpassword-routing.module';
import { ResetpasswordComponent } from './resetpassword.component';

import {InputNumberModule} from 'primeng/inputnumber';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { 
  WavesModule, ButtonsModule, 
  PreloadersModule,
  ProgressbarModule } from 'ng-uikit-pro-standard'
  import {PasswordModule} from 'primeng/password';
  import {DividerModule} from 'primeng/divider';

@NgModule({
  declarations: [ResetpasswordComponent],
  imports: [
    CommonModule,
    ResetpasswordRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonsModule,
    InputNumberModule,
    WavesModule,
    PreloadersModule,
    PasswordModule,
    ProgressbarModule,
    DividerModule
  ]
})
export class ResetpasswordModule { }
