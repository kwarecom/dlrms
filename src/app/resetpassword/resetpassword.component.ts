import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
// import { UserControllerService } from '../services/apis/services';
// import { AuthService } from '../services/auth-service';
import { AppServices } from '../services/app-services';
import { InputValidations } from '../services/validation';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.scss']
})
export class ResetpasswordComponent implements OnInit {

  user!: any;
  token!: any;
  count = 1;
  createdAt!: any;
  msgText!: string;
  showMsg!: boolean;
  success!: boolean;
  isLoading = false;

  confirmPassword!: string;
  newPassword!: string;
  passwordForm!: boolean;
  errMsgNewPassword!: string;
  errMsgConfirmPassword!: string;
  
  constructor(private route: ActivatedRoute, private service: AppServices, public validator: InputValidations,) { }

  ngOnInit(): void {
    this.service.setBrowserTabTitle('Password Reset');
    this.route.queryParams.subscribe((params: any) => {
      this.createdAt = params?.at;
      this.user = params?.u;
      this.token = params?.token;
      this.count = params?.c ? params.c : this.count;
  });
  }
  
verifyCode() {
        const elapsed = new Date().getTime() - Number(this.createdAt);
        this.isLoading = true;
        const diffHrs = Number((elapsed % 86400000) / 3600000).toFixed(2); // hours
        this.passwordForm =  !this.confirmPassword || !this.newPassword;
        this.errMsgNewPassword = !this.newPassword ? 'New password is required!' : !this.newPassword.match(this.validator.strongPasswordRegex()) ? 'Password is invalid!' : '';
        this.errMsgConfirmPassword = !this.confirmPassword ? 'Confirm password is required!' : this.newPassword.localeCompare(this.confirmPassword) !== 0 ? 'Password mismatched!' : '';
      
        if (Number(diffHrs) <= 0.5) {
          if (!this.passwordForm) {
            console.log(this.token);
            console.log(this.newPassword)
            const body ={resetKey: this.token.trim(), password: this.newPassword, confirmPassword: this.confirmPassword};
            // this.auth.resetPasswordFinish(body)
            // .then((res: any) => { this.auth.routeToPage('auth-page')}, (err: any) => {
            //   this.showMsg = true;
            //   this.msgText = 'Error occured while resetting password! Try again!';
            //   localStorage.removeItem('resetCount');
            //   setTimeout(() => {
            //     this.showMsg = false;
            //     this.isLoading = false;
            //   }, 5000);
            // })
          }
        } else {
          this.showMsg = true;
          this.msgText = 'Expired verification code!';
          setTimeout(() => {
            this.showMsg = false;
            this.isLoading = false;
          }, 5000);
        } 
      } 

resendEmail() {
  if(Number(this.count) + 1 > 3) {
    this.showMsg = true
    this.msgText = 'Resend request has reached daily limit for your account!';
    setTimeout(() => {
      this.showMsg = false;
      this.isLoading = false;
    }, 5000);
  } else {
    this.isLoading = true;
    // this.auth.resetPassword(this.user)
    // .then((res: any) => { 
    //   this.msgText = 'New password-reset email sent!';
    //   this.count += 1;
    //   this.success = true;
    //   this.showMsg = true;
    //   this.isLoading = false;
    //   setTimeout(() => {
    //     this.showMsg = false;
    //     this.success = false;
    //   }, 5000);
    // }, (err: any) => {
    //   console.log(err)
    //   this.showMsg = true
    //   this.msgText = 'Error sending new password-reset email! Try again';
    //   setTimeout(() => {
    //     this.showMsg = false;
    //     this.isLoading = false;
    //   }, 5000);
    // })
  }
}

handleNewPasswordBlur(e: any) {
  this.errMsgNewPassword = !e.target.value ? 'New password is required!' : !e.target.value.match(this.validator.strongPasswordRegex()) ? 'Password is invalid!' : '';
}
handleConfirmPasswordBlur(e: any) {
  this.errMsgConfirmPassword = !e.target.value ? 'Confirm password is required!' : this.newPassword.localeCompare(e.target.value) !== 0 ? 'Password mismatched!' : '';
}
}
