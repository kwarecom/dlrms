import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VerifyaccountComponent } from './verifyaccount.component';

const routes: Routes = [{path: '', component: VerifyaccountComponent}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VerifyaccountRoutingModule { }
