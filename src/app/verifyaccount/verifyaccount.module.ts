import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VerifyaccountRoutingModule } from './verifyaccount-routing.module';
import { VerifyaccountComponent } from './verifyaccount.component';
import {InputNumberModule} from 'primeng/inputnumber';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// MDB Angular Pro
import { 
  WavesModule, ButtonsModule, 
  PreloadersModule,
  ProgressbarModule } from 'ng-uikit-pro-standard'


@NgModule({
  declarations: [VerifyaccountComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    VerifyaccountRoutingModule,
    ButtonsModule,
    InputNumberModule,
    WavesModule,
    PreloadersModule,
    ProgressbarModule
  ],
  schemas: [NO_ERRORS_SCHEMA],
})
export class VerifyaccountModule { }
