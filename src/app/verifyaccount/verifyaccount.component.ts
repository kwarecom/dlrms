import { ThisReceiver } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
// import { UserControllerService } from '../services/apis/services';
// import { AuthService } from '../services/auth-service';
import { AppServices } from '../services/app-services';

@Component({
  selector: 'app-verifyaccount',
  templateUrl: './verifyaccount.component.html',
  styleUrls: ['./verifyaccount.component.scss']
})
export class VerifyaccountComponent implements OnInit {

  user!: any;
  token: any;
  count = 1;
  createdAt: any;
  msgText!: string;
  showMsg!: boolean;
  success!: boolean;
  isLoading = false;

  constructor(private route: ActivatedRoute, private service: AppServices) { }

  ngOnInit(): void {
    this.service.setBrowserTabTitle('Verify Account');
    this.handleInputChange();

    this.route.queryParams.subscribe((params: any) => {;
      this.user = params?.k;
      this.loadToken(params?.k);
  });
  }
handleInputChange() {
  const inputs: any = document.querySelectorAll('#codeInput > *[id]');
for (let i = 0; i < inputs.length; i++) { 
  inputs[i].addEventListener('keydown', function(event: any) { 
    if (event.key==="Backspace" ) { 
      // inputs[i].value = '' ; 
      if (i !== 0 ) {
        if (inputs[i].value === '') {
          inputs[i - 1].focus();
        } else {
          inputs[i].value = ''
        }} 
      } else { 
        if (i===inputs.length - 1 && inputs[i].value !=='' ) { return true; } 
        else if (event.keyCode> 47 && event.keyCode < 58) { inputs[i].value=event.key; 
          if (i !==inputs.length - 1 && inputs[i].value !== ' ') inputs[i + 1].focus(); event.preventDefault(); } 
          else if (event.keyCode> 64 && event.keyCode < 91) { inputs[i].value=String.fromCharCode(event.keyCode); 
            if (i !==inputs.length - 1 && inputs[i].value !== ' ') inputs[i + 1].focus(); event.preventDefault(); } } return; });
           };
}

verifyCode() {
  const elapsed = new Date().getTime() - Number(this.createdAt);
        this.isLoading = false;
        const formData: any = this.service.getFormObject('#codes');
        const initValue = formData.first;
        const diffHrs = Number((elapsed % 86400000) / 3600000).toFixed(2); // hours
        const inputCode = initValue.concat(formData.second, formData.third, formData.fourth, formData.fifth, formData.sixth);
     
        if (inputCode === this.token) {
        if (Number(diffHrs) <= 0.5) {
        //     this.userService.updateVerificationCode({id: this.user, body: {emailVerified: true, verificationToken: 0, resetCount: 0, resetTimestamp: new Date(), resetKeyTimestamp: new Date(), userStatus: 'Active', resetKey: ''}})
        //     .subscribe((res: any) => { this.auth.routeToPage('auth-page')}, (err: any) => {
        //       this.showMsg = true;
        //       this.msgText = 'Error occured while verifying your account! Try again!';
        //       setTimeout(() => {
        //         this.showMsg = false;
        //         this.isLoading = false;
        //       }, 7000);
        //     })
        // } else {
        //   this.showMsg = true;
        //   this.msgText = 'Expired verification code!';
        //   setTimeout(() => {
        //     this.showMsg = false;
        //     this.isLoading = false;
        //   }, 7000);
        } 
      } else {
          this.showMsg = true;
          this.msgText = 'Invalid verification code!';
          setTimeout(() => {
            this.showMsg = false;
            this.isLoading = false;
          }, 7000);
        }
}

resendCode() {
  if(Number(this.count) + 1 > 3) {
    this.showMsg = true
    this.msgText = 'Resend request has reached daily limit for your account!';
    setTimeout(() => {
      this.showMsg = false;
      this.isLoading = false;
    }, 7000);
  } else {
    this.isLoading = true;
    // this.userService.resendVerificationCode({id: this.user})
    // .subscribe((res: any) => { 
    //   this.msgText = 'New verification code sent!';
    //   this.count = Number(this.count) + 1;
    //   this.success = true;
    //   this.showMsg = true;
    //   this.isLoading = false;
    //   setTimeout(() => {
    //     this.showMsg = false;
    //     this.success = false;
    //   }, 7000);
    //   this.loadToken(this.user);
    // }, (err: any) => {
    //   this.showMsg = true;
    //   this.msgText = 'Request has reached daily limit! Try tomorrow';
    //   setTimeout(() => {
    //     this.showMsg = false;
    //     this.isLoading = false;
    //   }, 7000);
    // })
  }
}
loadToken(id: any) {
  // this.userService.findNewCode({newCodeId: id})
  // .subscribe((code: any) => {
  //   const data = JSON.parse(code);
  //   this.createdAt = new Date(data?.resetKeyTimestamp).getTime();
  //   this.token = data?.verificationToken.toString();
  //   this.count = data?.resetCount;
  // });
}
}
