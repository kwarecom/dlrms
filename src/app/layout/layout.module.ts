import { CommonModule } from '@angular/common';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  AccordionModule,
  ButtonsModule,
  IconsModule,
  NavbarModule,
  SidenavModule,
  TooltipModule,
  ModalModule,
  WavesModule
} from 'ng-uikit-pro-standard';
import { DoublenavComponent } from '../doublenav/doublenav.component';
import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import {DialogModule} from 'primeng/dialog';
import {ButtonModule} from 'primeng/button';
import {InputTextModule} from 'primeng/inputtext';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {InputNumberModule} from 'primeng/inputnumber';
import {CalendarModule} from 'primeng/calendar';
import {ProgressBarModule} from 'primeng/progressbar';
import {CheckboxModule} from 'primeng/checkbox';
import {ToastModule} from 'primeng/toast';
import { HttpClientModule } from '@angular/common/http';
import { DropdownModule } from "primeng/dropdown";
import {PasswordModule} from 'primeng/password';
import {FileUploadModule} from 'primeng/fileupload';
import { ImageCropperModule } from 'ngx-image-cropper';
import {AvatarModule} from 'primeng/avatar';

@NgModule({
  declarations: [DoublenavComponent, LayoutComponent],
  imports: [
    CommonModule,
    LayoutRoutingModule,
    FormsModule,
    HttpClientModule,
    IconsModule,
    ReactiveFormsModule,
    ButtonsModule,
    TooltipModule,
    SidenavModule,
    NavbarModule,
    ModalModule,
    WavesModule,
    AccordionModule,
    DialogModule,
    ButtonModule,
    InputTextModule,
    InputTextareaModule,
    InputNumberModule,
    CalendarModule,
    ProgressBarModule,
    CheckboxModule,
    ToastModule,
    PasswordModule,
    DropdownModule,
    FileUploadModule,
    ImageCropperModule,
    AvatarModule
  ],
  exports: [LayoutComponent, DoublenavComponent],
  schemas: [NO_ERRORS_SCHEMA],
})
export class LayoutModule {}
