import { AfterViewInit, Component, OnInit } from '@angular/core';
import { AppServices } from '../services/app-services';
import { InputValidations } from '../services/validation';

@Component({
  selector: 'app-deeds',
  templateUrl: './deeds.component.html',
  styleUrls: ['./deeds.component.scss']
})
export class DeedsComponent implements OnInit, AfterViewInit {

  instrumentType: any[] = [
    {name: 'Deed', code: 1},
    {name: 'Non-Deed', code: 2}
  ]
  county: any[] = [
    { name: 'Bomi', code: 'BO' },
    { name: 'Bong', code: 'BG' },
    { name: 'Gbarpolu', code: 'GB' }
  ]
  constructor( public validator: InputValidations,
    private service: AppServices,) { }

  ngOnInit(): void {
    this.service.setBrowserTabTitle('Deeded Transactions');
  }
  ngAfterViewInit() {
    this.service.pageLoader();
  }
}
