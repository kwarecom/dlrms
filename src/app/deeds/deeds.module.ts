import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {InputTextModule} from 'primeng/inputtext';
import {DropdownModule} from 'primeng/dropdown';

import { DeedsRoutingModule } from './deeds-routing.module';
import { DeedsComponent } from './deeds.component';
import { LayoutModule } from '../layout/layout.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [DeedsComponent],
  imports: [
    CommonModule,
    DeedsRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    InputTextModule,
    DropdownModule,
    LayoutModule
  ]
})
export class DeedsModule { }
