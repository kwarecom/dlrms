import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DeedsComponent } from './deeds.component';

const routes: Routes = [{path: '', component: DeedsComponent}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeedsRoutingModule { }
