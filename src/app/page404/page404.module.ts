import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Page404RoutingModule } from './page404-routing.module';
import { Page404Component } from './page404.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [Page404Component],
  imports: [
    CommonModule,
    Page404RoutingModule,
    ReactiveFormsModule
  ]
})
export class Page404Module { }
