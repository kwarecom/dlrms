import { Component, OnInit } from '@angular/core';
import { FormGroup, AbstractControl, FormControl, Validators } from '@angular/forms';
import { Location } from '@angular/common';

declare var $: any;

@Component({
  selector: 'app-page404',
  templateUrl: './page404.component.html',
  styleUrls: ['./page404.component.scss']
})
export class Page404Component implements OnInit {

  searchForm!: FormGroup;
  searchInput!: AbstractControl;

  constructor(public location: Location) { }

  ngOnInit(): void {
    $('#home-page').removeClass('active');
    this.initSearchForm();
  }

initSearchForm(): void {
  this.searchForm = new FormGroup({
    searchText: this.searchInput = new FormControl('',
    [
      Validators.nullValidator
    ]
    )
  });
}
}
