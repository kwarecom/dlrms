import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SuggestionboxRoutingModule } from './suggestionbox-routing.module';
import { SuggestionboxComponent } from './suggestionbox.component';


@NgModule({
  declarations: [SuggestionboxComponent],
  imports: [
    CommonModule,
    SuggestionboxRoutingModule
  ]
})
export class SuggestionboxModule { }
