import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SuggestionboxComponent } from './suggestionbox.component';

describe('SuggestionboxComponent', () => {
  let component: SuggestionboxComponent;
  let fixture: ComponentFixture<SuggestionboxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SuggestionboxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SuggestionboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
