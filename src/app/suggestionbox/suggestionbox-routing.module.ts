import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SuggestionboxComponent } from './suggestionbox.component';

const routes: Routes = [{path: '', component: SuggestionboxComponent}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SuggestionboxRoutingModule { }
