import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthComponent } from './auth/auth.component';

const routes: Routes = [
  { path: '', redirectTo: '/auth-page', pathMatch: 'full' },
  { path: 'auth-page', component: AuthComponent },
  {
    path: 'dashboard',
    loadChildren: () => import('./home/home.module').then(m => m.HomeModule)
  },
  {
    path: 'deeded-transactions',
    loadChildren: () => import('./deeds/deeds.module').then(m => m.DeedsModule)
  },
  {
    path: 'help',
    loadChildren: () => import('./help/help.module').then(m => m.HelpModule)
  },
  {
    path: 'non-deeded-transactions',
    loadChildren: () => import('./nondeeds/nondeeds.module').then(m => m.NondeedsModule)
  },
  {
    path: 'spatial-information',
    loadChildren: () => import('./spatial/spatial.module').then(m => m.SpatialModule)
  },
  {
    path: 'suggestion-box',
    loadChildren: () => import('./suggestionbox/suggestionbox.module').then(m => m.SuggestionboxModule)
  },
  {
    path: 'verify-account',
    loadChildren: () => import('./verifyaccount/verifyaccount.module').then(m => m.VerifyaccountModule)
  },
  {
    path: 'reset-password',
    loadChildren: () => import('./resetpassword/resetpassword.module').then(m => m.ResetpasswordModule)
  },
  {
    path: "**",
    loadChildren: () =>
      import("./page404/page404.module").then((m) => m.Page404Module),
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
