import { AfterViewInit, Component, OnInit } from '@angular/core';
import { AppServices } from '../services/app-services';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, AfterViewInit {

  constructor(private service: AppServices) { }

  ngOnInit(): void {
    this.service.setBrowserTabTitle('Home Page');
  }
  ngAfterViewInit() {
    this.service.pageLoader();
  }
}
